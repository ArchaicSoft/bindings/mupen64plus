module m64p_plugin;

import m64p_types;

static extern(C):

    /*** Controller plugin's ****/
    enum PLUGIN_NONE         = 1;
    enum PLUGIN_MEMPAK       = 2;
    enum PLUGIN_RUMBLE_PAK   = 3; /* not implemented for non raw data */
    enum PLUGIN_TRANSFER_PAK = 4; /* not implemented for non raw data */
    enum PLUGIN_RAW          = 5; /* the controller plugin is passed in raw data */
    enum PLUGIN_BIO_PAK      = 6;

    /***** Structures *****/
    struct RSP_INFO
    {
        ubyte* RDRAM;
        ubyte* DMEM;
        ubyte* IMEM;

        uint* MI_INTR_REG;

        uint* SP_MEM_ADDR_REG;
        uint* SP_DRAM_ADDR_REG;
        uint* SP_RD_LEN_REG;
        uint* SP_WR_LEN_REG;
        uint* SP_STATUS_REG;
        uint* SP_DMA_FULL_REG;
        uint* SP_DMA_BUSY_REG;
        uint* SP_PC_REG;
        uint* SP_SEMAPHORE_REG;

        uint* DPC_START_REG;
        uint* DPC_END_REG;
        uint* DPC_CURRENT_REG;
        uint* DPC_STATUS_REG;
        uint* DPC_CLOCK_REG;
        uint* DPC_BUFBUSY_REG;
        uint* DPC_PIPEBUSY_REG;
        uint* DPC_TMEM_REG;

        void function() CheckInterrupts;
        void function() ProcessDlistList;
        void function() ProcessAlistList;
        void function() ProcessRdpList;
        void function() ShowCFB;
    }

    struct GFX_INFO
    {
        ubyte* HEADER;  /* This is the rom header (first 40h bytes of the rom) */
        ubyte* RDRAM;
        ubyte* DMEM;
        ubyte* IMEM;

        uint* MI_INTR_REG;

        uint* DPC_START_REG;
        uint* DPC_END_REG;
        uint* DPC_CURRENT_REG;
        uint* DPC_STATUS_REG;
        uint* DPC_CLOCK_REG;
        uint* DPC_BUFBUSY_REG;
        uint* DPC_PIPEBUSY_REG;
        uint* DPC_TMEM_REG;

        uint* VI_STATUS_REG;
        uint* VI_ORIGIN_REG;
        uint* VI_WIDTH_REG;
        uint* VI_INTR_REG;
        uint* VI_V_CURRENT_LINE_REG;
        uint* VI_TIMING_REG;
        uint* VI_V_SYNC_REG;
        uint* VI_H_SYNC_REG;
        uint* VI_LEAP_REG;
        uint* VI_H_START_REG;
        uint* VI_V_START_REG;
        uint* VI_V_BURST_REG;
        uint* VI_X_SCALE_REG;
        uint* VI_Y_SCALE_REG;

        void function() CheckInterrupts;

        /* The GFX_INFO.version parameter was added in version 2.5.1 of the core.
        Plugins should ensure the core is at least this version before
        attempting to read GFX_INFO.version. */
        uint Version;
        /* SP_STATUS_REG and RDRAM_SIZE were added in version 2 of GFX_INFO.version.
        Plugins should only attempt to read these values if GFX_INFO.version is at least 2. */

        /* The RSP plugin should set (HALT | BROKE | TASKDONE) *before* calling ProcessDList.
        It should not modify SP_STATUS_REG after ProcessDList has returned.
        This will allow the GFX plugin to unset these bits if it needs. */
        uint* SP_STATUS_REG;
        const uint* RDRAM_SIZE;
    }

    struct AUDIO_INFO
    {
        ubyte* RDRAM;
        ubyte* DMEM;
        ubyte* IMEM;

        uint* MI_INTR_REG;

        uint* AI_DRAM_ADDR_REG;
        uint* AI_LEN_REG;
        uint* AI_CONTROL_REG;
        uint* AI_STATUS_REG;
        uint* AI_DACRATE_REG;
        uint* AI_BITRATE_REG;

        void function() CheckInterrupts;
    }

    struct CONTROL
    {
        int Present;
        int RawData;
        int Plugin;
    }

    union BUTTONS
    {
        uint Value;
        
        import std.bitmanip : bitfields;
        mixin(bitfields!(
            uint, "uR_DPAD",       1,
            uint, "uL_DPAD",       1,
            uint, "uD_DPAD",       1,
            uint, "uU_DPAD",       1,
            uint, "uSTART_BUTTON", 1,
            uint, "uZ_TRIG",       1,
            uint, "uB_BUTTON",     1,
            uint, "uA_BUTTON",     1,

            uint, "uR_CBUTTON",    1,
            uint, "uL_CBUTTON",    1,
            uint, "uD_CBUTTON",    1,
            uint, "uU_CBUTTON",    1,
            uint, "uR_TRIG",       1,
            uint, "uL_TRIG",       1,
            uint, "uReserved1",    1,
            uint, "uReserved2",    1,

            int,  "X_AXIS",        8,
            int,  "Y_AXIS",        8
        ));
    }

    struct CONTROL_INFO
    {
        CONTROL *Controls;      /* A pointer to an array of 4 controllers .. eg:
                                CONTROL Controls[4]; */
    }

    /* common plugin function pointer types */
    int function() RomOpen;
    void function() RomClosed;

    /* video plugin function pointer types */
    void function() ChangeWindow;
    int  function(GFX_INFO Gfx_Info) InitiateGFX;
    void function(int x, int y) MoveScreen;
    void function() ProcessDList;
    void function() ProcessRDPList;
    void function() ShowCFB;
    void function() UpdateScreen;
    void function() ViStatusChanged;
    void function() ViWidthChanged;
    void function(void* dest, int* width, int* height, int front) ReadScreen2;
    void function(void function(int) callback) SetRenderingCallback;
    void function(int width, int height) ResizeVideoOutput;

    /* frame buffer plugin spec extension */
    struct FrameBufferInfo
    {
        uint addr;
        uint size;
        uint width;
        uint height;
    }

    void function(uint addr) FBRead;
    void function(uint addr, uint size) FBWrite;
    void function(void* p) FBGetFrameBufferInfo;

    /* audio plugin function pointers */
    void function(int SystemType) AiDacrateChanged;
    void function() AiLenChanged;
    int  function(AUDIO_INFO Audio_Info) InitiateAudio;
    void function() ProcessAList;
    void function(int percent) SetSpeedFactor;
    void function() VolumeUp;
    void function() VolumeDown;
    int  function() VolumeGetLevel;
    void function(int level) VolumeSetLevel;
    void function() VolumeMute;
    const(char)* function() VolumeGetString;

    /* input plugin function pointers */
    void function(int Control, ubyte* Command) ControllerCommand;
    void function(int Control, BUTTONS* Keys) GetKeys;
    void function(CONTROL_INFO ControlInfo) InitiateControllers;
    void function(int Control, ubyte* Command) ReadController;
    void function(int keymod, int keysym) SDL_KeyDown;
    void function(int keymod, int keysym) SDL_KeyUp;
    void function() RenderCallback;

    /* RSP plugin function pointers */
    uint function(uint Cycles) DoRspCycles;
    void function(RSP_INFO Rsp_Info, uint* CycleCount) InitiateRSP;
