module m64p;

import dynalib;

import std.stdio : writeln;

private static m64p_handle coreHandle;
private static m64p_handle audioHandle;
private static m64p_handle inputHandle;
private static m64p_handle rspHandle;
private static m64p_handle videoHandle;

public:
    import m64p_common;
    import m64p_conditionals;
    import m64p_config;
    import m64p_debugger;
    import m64p_frontend;
    import m64p_plugin;
    import m64p_types;
    import m64p_vidext;

    m64p_handle m64p_coreHandle() { return coreHandle; }
    bool m64p_coreIsLoaded() { return coreHandle != null; }

    m64p_error m64p_loadCore()
    {
        if (m64p_coreIsLoaded) return M64ERR_SUCCESS;

        DynaLib lib;
        if (!lib.loadFromConfig("mupen64plus", "mupen64plus-core"))
        {
            writeln("There was a problem loading mupen64plus-core");
            return M64ERR_NOT_INIT;
        } coreHandle = lib.handle;

        m64p_coreBind(&lib);
    
        return M64ERR_SUCCESS;
    }

    m64p_error m64p_loadCore(string libDir = "", string depDir = "!")
    {
        if (m64p_coreIsLoaded) return M64ERR_SUCCESS;

        DynaLib lib;
        if (!lib.load("mupen64plus", libDir, depDir))
        {
            writeln("There was a problem loading mupen64plus-core");
            return M64ERR_NOT_INIT;
        } coreHandle = lib.handle;

        m64p_coreBind(&lib);
    
        return M64ERR_SUCCESS;
    }

    private void m64p_coreBind(DynaLib* lib)
    {
        import m64p_conditionals;

        /* Common */
        lib.bind(&PluginGetVersion,"PluginGetVersion");
        lib.bind(&CoreGetAPIVersions,"CoreGetAPIVersions");
        lib.bind(&CoreErrorMessage,"CoreErrorMessage");
            
        /* Config */
        lib.bind(&ConfigListSections,"ConfigListSections");
        lib.bind(&ConfigOpenSection,"ConfigOpenSection");
        lib.bind(&ConfigListParameters,"ConfigListParameters");
        lib.bind(&ConfigSaveFile,"ConfigSaveFile");
        lib.bind(&ConfigSaveSection,"ConfigSaveSection");
        lib.bind(&ConfigHasUnsavedChanges,"ConfigHasUnsavedChanges");
        lib.bind(&ConfigDeleteSection,"ConfigDeleteSection");
        lib.bind(&ConfigRevertChanges,"ConfigRevertChanges");
        lib.bind(&ConfigSetParameter,"ConfigSetParameter");
        lib.bind(&ConfigSetParameterHelp,"ConfigSetParameterHelp");
        lib.bind(&ConfigGetParameter,"ConfigGetParameter");
        lib.bind(&ConfigGetParameterType,"ConfigGetParameterType");
        lib.bind(&ConfigGetParameterHelp,"ConfigGetParameterHelp");
        lib.bind(&ConfigSetDefaultInt,"ConfigSetDefaultInt");
        lib.bind(&ConfigSetDefaultFloat,"ConfigSetDefaultFloat");
        lib.bind(&ConfigSetDefaultBool,"ConfigSetDefaultBool");
        lib.bind(&ConfigSetDefaultString,"ConfigSetDefaultString");
        lib.bind(&ConfigGetParamInt,"ConfigGetParamInt");
        lib.bind(&ConfigGetParamFloat,"ConfigGetParamFloat");
        lib.bind(&ConfigGetParamBool,"ConfigGetParamBool");
        lib.bind(&ConfigGetParamString,"ConfigGetParamString");
        lib.bind(&ConfigGetSharedDataFilepath,"ConfigGetSharedDataFilepath");
        lib.bind(&ConfigGetUserConfigPath,"ConfigGetUserConfigPath");
        lib.bind(&ConfigGetUserDataPath,"ConfigGetUserDataPath");
        lib.bind(&ConfigGetUserCachePath,"ConfigGetUserCachePath");
        lib.bind(&ConfigExternalOpen,"ConfigExternalOpen");
        lib.bind(&ConfigExternalClose,"ConfigExternalClose");
        lib.bind(&ConfigExternalGetParameter,"ConfigExternalGetParameter");
        lib.bind(&ConfigSendNetplayConfig,"ConfigSendNetplayConfig");
        lib.bind(&ConfigReceiveNetplayConfig,"ConfigReceiveNetplayConfig");
        lib.bind(&ConfigOverrideUserPaths,"ConfigOverrideUserPaths");
            
        /* Debug */
        lib.bind(&DebugSetCallbacks,"DebugSetCallbacks");
        lib.bind(&DebugSetCoreCompare,"DebugSetCoreCompare");
        lib.bind(&DebugSetRunState,"DebugSetRunState");
        lib.bind(&DebugGetState,"DebugGetState");
        lib.bind(&DebugStep,"DebugStep");
        lib.bind(&DebugDecodeOp,"DebugDecodeOp");
        lib.bind(&DebugMemGetRecompInfo,"DebugMemGetRecompInfo");
        lib.bind(&DebugMemGetMemInfo,"DebugMemGetMemInfo");
        lib.bind(&DebugMemGetPointer,"DebugMemGetPointer");
        lib.bind(&DebugMemRead64,"DebugMemRead64");
        lib.bind(&DebugMemRead32,"DebugMemRead32");
        lib.bind(&DebugMemRead16,"DebugMemRead16");
        lib.bind(&DebugMemRead8,"DebugMemRead8");
        lib.bind(&DebugMemWrite64,"DebugMemWrite64");
        lib.bind(&DebugMemWrite32,"DebugMemWrite32");
        lib.bind(&DebugMemWrite16,"DebugMemWrite16");
        lib.bind(&DebugMemWrite8,"DebugMemWrite8");
        lib.bind(&DebugGetCPUDataPtr,"DebugGetCPUDataPtr");
        lib.bind(&DebugBreakpointLookup,"DebugBreakpointLookup");
        lib.bind(&DebugBreakpointCommand,"DebugBreakpointCommand");
        lib.bind(&DebugBreakpointTriggeredBy,"DebugBreakpointTriggeredBy");
        lib.bind(&DebugVirtualToPhysical,"DebugVirtualToPhysical");
            
        /* Front-End */
        lib.bind(&CoreStartup,"CoreStartup");
        lib.bind(&CoreShutdown,"CoreShutdown");
        lib.bind(&CoreAttachPlugin,"CoreAttachPlugin");
        lib.bind(&CoreDetachPlugin,"CoreDetachPlugin");
        lib.bind(&CoreDoCommand,"CoreDoCommand");
        lib.bind(&CoreOverrideVidExt,"CoreOverrideVidExt");
        lib.bind(&CoreAddCheat,"CoreAddCheat");
        lib.bind(&CoreCheatEnabled,"CoreCheatEnabled");
        lib.bind(&CoreGetRomSettings,"CoreGetRomSettings");

        if (
            lib.bind(&CoreSaveOverride,"CoreSaveOverride") &&
            lib.bind(&GetHeader,"GetHeader") &&
            lib.bind(&GetRdRam,"GetRdRam") &&
            lib.bind(&GetRom,"GetRom") &&
            lib.bind(&GetRdRamSize,"GetRdRamSize") &&
            lib.bind(&GetRomSize,"GetRomSize") &&
            lib.bind(&RefreshDynarec,"RefreshDynarec")
        ) {
            version(DL_NOTIFICATION)
                writeln("Dynalib-Conditional: [M64P] Modding supported.");
            (cast(bool*)&conditional_Modding)[0] = true;
        } else (cast(bool*)&conditional_Modding)[0] = false;

        /* Plugin */

        /* VidExt */
        lib.bind(&VidExt_Init,"VidExt_Init");
        lib.bind(&VidExt_Quit,"VidExt_Quit");
        lib.bind(&VidExt_ListFullscreenModes,"VidExt_ListFullscreenModes");
        lib.bind(&VidExt_ListFullscreenRates,"VidExt_ListFullscreenRates");
        lib.bind(&VidExt_SetVideoMode,"VidExt_SetVideoMode");
        lib.bind(&VidExt_SetVideoModeWithRate,"VidExt_SetVideoModeWithRate");
        lib.bind(&VidExt_ResizeWindow,"VidExt_ResizeWindow");
        lib.bind(&VidExt_SetCaption,"VidExt_SetCaption");
        lib.bind(&VidExt_ToggleFullScreen,"VidExt_ToggleFullScreen");
        lib.bind(&VidExt_GL_GetProcAddress,"VidExt_GL_GetProcAddress");
        lib.bind(&VidExt_GL_SetAttribute,"VidExt_GL_SetAttribute");
        lib.bind(&VidExt_GL_GetAttribute,"VidExt_GL_GetAttribute");
        lib.bind(&VidExt_GL_SwapBuffers,"VidExt_GL_SwapBuffers");
        lib.bind(&VidExt_GL_GetDefaultFramebuffer,"VidExt_GL_GetDefaultFramebuffer");
    }

    m64p_error m64p_unloadCore()
    {
        if (!m64p_coreIsLoaded) return M64ERR_NOT_INIT;

        /* In case plugins are attached */
        m64p_detachPlugins();

        unloadLib(coreHandle);
        coreHandle = null;

        /* Common */
        PluginGetVersion = null;
        CoreGetAPIVersions = null;
        CoreErrorMessage = null;
        PluginStartup = null;
        PluginShutdown = null;

        /* Config */
        ConfigListSections = null;
        ConfigOpenSection = null;
        ConfigListParameters = null;
        ConfigSaveFile = null;
        ConfigSaveSection = null;
        ConfigHasUnsavedChanges = null;
        ConfigDeleteSection = null;
        ConfigRevertChanges = null;
        ConfigSetParameter = null;
        ConfigSetParameterHelp = null;
        ConfigGetParameter = null;
        ConfigGetParameterType = null;
        ConfigGetParameterHelp = null;
        ConfigSetDefaultInt = null;
        ConfigSetDefaultFloat = null;
        ConfigSetDefaultBool = null;
        ConfigSetDefaultString = null;
        ConfigGetParamInt = null;
        ConfigGetParamFloat = null;
        ConfigGetParamBool = null;
        ConfigGetParamString = null;
        ConfigGetSharedDataFilepath = null;
        ConfigGetUserConfigPath = null;
        ConfigGetUserDataPath = null;
        ConfigGetUserCachePath = null;
        ConfigExternalOpen = null;
        ConfigExternalClose = null;
        ConfigExternalGetParameter = null;
        ConfigSendNetplayConfig = null;
        ConfigReceiveNetplayConfig = null;
        ConfigOverrideUserPaths = null;

        /* Debug */
        DebugSetCallbacks = null;
        DebugSetCoreCompare = null;
        DebugSetRunState = null;
        DebugGetState = null;
        DebugStep = null;
        DebugDecodeOp = null;
        DebugMemGetRecompInfo = null;
        DebugMemGetMemInfo = null;
        DebugMemGetPointer = null;
        DebugMemRead64 = null;
        DebugMemRead32 = null;
        DebugMemRead16 = null;
        DebugMemRead8 = null;
        DebugMemWrite64 = null;
        DebugMemWrite32 = null;
        DebugMemWrite16 = null;
        DebugMemWrite8 = null;
        DebugGetCPUDataPtr = null;
        DebugBreakpointLookup = null;
        DebugBreakpointCommand = null;
        DebugBreakpointTriggeredBy = null;
        DebugVirtualToPhysical = null;

        /* Front-End */
        CoreStartup = null;
        CoreShutdown = null;
        CoreAttachPlugin = null;
        CoreDetachPlugin = null;
        CoreDoCommand = null;
        CoreOverrideVidExt = null;
        CoreAddCheat = null;
        CoreCheatEnabled = null;
        if (conditional_Modding)
        {
            CoreSaveOverride = null;
            GetHeader = null;
            GetRdRam = null;
            GetRom = null;
            GetRdRamSize = null;
            GetRomSize = null;
            RefreshDynarec = null;
        }

        /* Plugin */

        /* VidExt */
        VidExt_Init = null;
        VidExt_Quit = null;
        VidExt_ListFullscreenModes = null;
        VidExt_ListFullscreenRates = null;
        VidExt_SetVideoMode = null;
        VidExt_SetVideoModeWithRate = null;
        VidExt_ResizeWindow = null;
        VidExt_SetCaption = null;
        VidExt_ToggleFullScreen = null;
        VidExt_GL_GetProcAddress = null;
        VidExt_GL_SetAttribute = null;
        VidExt_GL_GetAttribute = null;
        VidExt_GL_SwapBuffers = null;
        VidExt_GL_GetDefaultFramebuffer = null;

        return M64ERR_SUCCESS;
    }

    bool m64p_pluginIsLoaded(m64p_plugin_type type)
    {
        switch (type)
        {
            case M64PLUGIN_CORE: return coreHandle != null;
            case M64PLUGIN_AUDIO: return audioHandle != null;
            case M64PLUGIN_INPUT: return inputHandle != null;
            case M64PLUGIN_RSP: return rspHandle != null;
            case M64PLUGIN_GFX: return videoHandle != null;
            default: return false;
        }
    }

    m64p_error m64p_attachPlugins(DebugCallback debugCallback,
                                  string videoPlugin, string audioPlugin,
                                  string inputPlugin, string rspPlugin,
                                  string libDir = "", string depDir = "!")
    {
        auto success =
        (
            (m64p_loadPlugin(&videoHandle, M64PLUGIN_GFX, debugCallback,
                libDir, videoPlugin, depDir) == M64ERR_SUCCESS) &&
            (m64p_loadPlugin(&audioHandle, M64PLUGIN_AUDIO, debugCallback,
                libDir, audioPlugin, depDir) == M64ERR_SUCCESS) &&
            (m64p_loadPlugin(&inputHandle, M64PLUGIN_INPUT, debugCallback,
                libDir, inputPlugin, depDir) == M64ERR_SUCCESS) &&
            (m64p_loadPlugin(&rspHandle, M64PLUGIN_RSP, debugCallback,
                libDir, rspPlugin, depDir) == M64ERR_SUCCESS)
        );
        
        /* Successfully loaded all plugins */
        if (success) return M64ERR_SUCCESS;

        /* Failed horribly! Revert! */
        m64p_detachPlugins();
        return M64ERR_PLUGIN_FAIL;
    }

    void m64p_detachPlugins()
    {
        m64p_unloadPlugin(&rspHandle);
        m64p_unloadPlugin(&inputHandle);
        m64p_unloadPlugin(&audioHandle);
        m64p_unloadPlugin(&videoHandle);
    }

    private m64p_error m64p_loadPlugin(m64p_handle* plug, m64p_plugin_type type, DebugCallback callback,
                                       string path, string name, string depsPath = "!")
    {
        /* Check if core is attached */
        if (coreHandle == null) return M64ERR_NOT_INIT;

        /* Check if already attached */
        if (*plug != null) return M64ERR_INVALID_STATE;

        /* Load the lib */
        DynaLib lib; lib.load(name, path, depsPath);
        if (!lib.isLoaded)
        {
            writeln("Mupen64Plus: failed to find ", name);
            return M64ERR_INPUT_NOT_FOUND;
        }

        /* Initialize the plugin */
        if (lib.bind(&PluginStartup, "PluginStartup"))
        {
            PluginStartup(coreHandle, cast(void*) (name ~ 0x00), callback);
            PluginStartup = null;
        } else return M64ERR_NOT_INIT;

        /* Attempt attaching it to core lib */
        if (CoreAttachPlugin(type, lib.handle) != M64ERR_SUCCESS)
        {
            lib.unload();
            return M64ERR_NOT_INIT;
        }

        *plug = lib.handle;
        return M64ERR_SUCCESS;
    }

    private m64p_error m64p_unloadPlugin(m64p_handle* plug)
    {
        if (coreHandle == null) return M64ERR_NOT_INIT;

        /* Check if not already attached */
        if (*plug == null) return M64ERR_INVALID_STATE;
            
        /* Shutdown the plugin */
        bindSymbol(*plug, cast(void**)&PluginShutdown,"PluginShutdown");
        PluginShutdown();
        PluginShutdown = null;

        /* Detach the lib */
        unloadLib(*plug);
        *plug = null;

        return M64ERR_SUCCESS;
    }
