module m64p_common;

import m64p_frontend;
import m64p_types;

static extern(C):

    /* PluginGetVersion()
    *
    * This function retrieves version information from a library. This
    * function is the same for the core library and the plugins.
    */
    m64p_error function(m64p_plugin_type*, int*, int*, const char**, int*) PluginGetVersion;
            
    /* CoreGetAPIVersions()
    *
    * This function retrieves API version information from the core.
    */
    m64p_error function(int*, int*, int*, int*) CoreGetAPIVersions;
            
    /* CoreErrorMessage()
    *
    * This function returns a pointer to a NULL-terminated string giving a
    * human-readable description of the error.
    */
    string function(m64p_error) CoreErrorMessage;

    /* PluginStartup()
    *
    * This function initializes a plugin for use by allocating memory, creating
    * data structures, and loading the configuration data.
    */
    m64p_error function(m64p_dynlib_handle, void*, DebugCallback) PluginStartup;

    /* PluginShutdown()
    *
    * This function destroys data structures and releases memory allocated by
    * the plugin library.
    */
    m64p_error function() PluginShutdown;
